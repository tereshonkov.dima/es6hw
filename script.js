class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    getName() {
       return this.name;
    }
    setName(newName) {
        if (newName.length > 0) {
            this.name = newName;
        }
    }
    getAge() {
        return this.age;
    }
    setAge(newAge) {
        if (newAge > 0) {
            this.age = newAge;
        }
    }
    getSalary() {
        return this.salary;
    }
    setSalary(newSalary) {
        if (newSalary > 0) {
            this.salary = newSalary;
        }
    }
}


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name,age,salary);
        this.lang = lang;
    }
    getNewSalary() {
        return this.salary * 3;
    }
}

const card1 = new Programmer("Dmitro", 33, 32000, "javascript");
console.log(card1);
const card2 = new Programmer("Margarita", 31, 10000, "java");
console.log(card2);
const card3 = new Programmer("Ivan", 18, 20000, "C++");
console.log(card3);
const newSalary = card1.getNewSalary();
console.log(newSalary);

